function varargout = gehoertest_ui(varargin)
% GEHOERTEST_UI MATLAB code for gehoertest_ui.fig
%      GEHOERTEST_UI, by itself, creates a new GEHOERTEST_UI or raises the existing
%      singleton*.
%
%      H = GEHOERTEST_UI returns the handle to a new GEHOERTEST_UI or the handle to
%      the existing singleton*.
%
%      GEHOERTEST_UI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in GEHOERTEST_UI.M with the given input arguments.
%
%      GEHOERTEST_UI('Property','Value',...) creates a new GEHOERTEST_UI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before gehoertest_ui_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to gehoertest_ui_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gehoertest_ui

% Last Modified by GUIDE v2.5 28-Jan-2016 10:27:18

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @gehoertest_ui_OpeningFcn, ...
                   'gui_OutputFcn',  @gehoertest_ui_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gehoertest_ui is made visible.
function gehoertest_ui_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to gehoertest_ui (see VARARGIN)

% Choose default command line output for gehoertest_ui
handles.output = hObject;
global handler;
handler(1) = handles.textAp0;
handler(2) = handles.textPhon;
handler(3) = handles.editAp0;
handler(4) = handles.editPhon;
handler(5) = handles.buttonStart;
handler(6) = handles.textLog;
handler(7) = handles.sliderPhon;
handler(8) = handles.axesPlot;
handler(9) = handles.buttonPlay;
handler(10) = handles.buttonNext;

initialize();

% Disable all objects:
set(handler(7),'Enable','off');
set(handler(9),'Enable','off');
set(handler(10),'Enable','off');
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gehoertest_ui wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gehoertest_ui_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on slider movement.
function sliderPhon_Callback(hObject, eventdata, handles)
% hObject    handle to sliderPhon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
global Phon_current
num = get(hObject,'Value');
Phon_current = num*120;


% --- Executes during object creation, after setting all properties.
function sliderPhon_CreateFcn(hObject, eventdata, handles)
% hObject    handle to sliderPhon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end



function editAp0_Callback(hObject, eventdata, handles)
% hObject    handle to editAp0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
num = str2double(get(hObject,'String'));
if isnan(num)
    num = 0;
    set(hObject,'String',num);
    errordlg('Input must be a number', 'Error')
end
handles.editAp0 = num;
guidata(hObject,handles) 
% Hints: get(hObject,'String') returns contents of editAp0 as text
%        str2double(get(hObject,'String')) returns contents of editAp0 as a double


% --- Executes during object creation, after setting all properties.
function editAp0_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editAp0 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject,'String','2.5E-04');
guidata(hObject,handles)



function editPhon_Callback(hObject, eventdata, handles)
% hObject    handle to editPhon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
num = str2double(get(hObject,'String'));
if isnan(num)
    num = 0;
    set(hObject,'String',num);
    errordlg('Input must be a number', 'Error')
end
handles.editPhon= num;
guidata(hObject,handles) 
% Hints: get(hObject,'String') returns contents of editPhon as text
%        str2double(get(hObject,'String')) returns contents of editPhon as a double


% --- Executes during object creation, after setting all properties.
function editPhon_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editPhon (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
set(hObject,'String','40');
guidata(hObject,handles)


% --- Executes on button press in buttonStart.
function buttonStart_Callback(hObject, eventdata, handles)
% hObject    handle to buttonStart (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global handler Stage A_p0 A_1kHz Phon A Phon_current;
A_p0 = str2double(get(handler(3),'String'));
Phon = str2double(get(handler(4),'String'));
currentStatus = get(hObject,'String');

if strcmp(currentStatus,'Start')
     if A_p0 > 0 && Phon >= 0
        set(handler(3),'Enable','off');
        set(handler(4),'Enable','off');
        set(hObject,'String', 'Stop');
        A_1kHz = A_p0*10^(Phon/20);
        Phon_current = Phon;
        setSliderPhon();
        Stage = 1;
        playSound();
     end
elseif strcmp(currentStatus,'Stop')
    set(handler(3),'Enable','on');
    set(handler(4),'Enable','on');
    set(handler(7),'Enable','off');
    set(handler(9),'Enable','off');
    set(handler(10),'Enable','off');
    set(hObject,'String', 'Start');
end


% --- Executes on button press in buttonPlay.
function buttonPlay_Callback(hObject, eventdata, handles)
% hObject    handle to buttonPlay (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
playSound();


% --- Executes on button press in buttonNext.
function buttonNext_Callback(hObject, eventdata, handles)
% hObject    handle to buttonNext (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
global Stage A A_1kHz Phon_current Phon f_axis A_array handler;
n = length(f_axis);
if Stage == n
    A_array(Stage)=A; % Save measured value of equal loudness
    logText('Done Testing...');
    plotDiagramm();
    set(handler(3),'Enable','off');
    set(handler(4),'Enable','off');
    set(handler(5),'String', 'Start');
    set(handler(7),'Enable','off');
    set(handler(9),'Enable','off');
    set(handler(10),'Enable','off');
    %resetProgramm();
else
    A_array(Stage)=A; % Save measured value of equal loudness
    A = A_1kHz;
    Phon_current = Phon;
    Stage = Stage + 1;
    setSliderPhon();
    playSound();
end

