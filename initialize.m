function initialize()

    global f_axis A_1kHz Phon FS t envelope
    
    Phon = 40;
    
    %Define frequencies at which hearing threshold shall be measured
    f_axis = [63 125 250 500 1000 2000 3000 4000 5000 6000 8000 10000];
    
    FS=44100; %Sampling frequency
    t=0:1/FS:1; % time vector: test tone length is one second
    envelope=tukeywin(length(t),0.25)';%H?llkurve f?r fading in and out
    
    
end