function resetProgramm()
global handler Stage A_p0 A_1kHz Phon A Phon_current A_array;

clear A_array;

% Disable all objects:
set(handler(7),'Enable','off');
set(handler(9),'Enable','off');
set(handler(10),'Enable','off');

end