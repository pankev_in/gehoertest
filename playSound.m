function playSound()
    global handler f_axis A_1kHz FS t envelope A_p0 A Phon Stage y_1kHz Phon_current
    
    % Disable all objects:
    set(handler(7),'Enable','off');
    set(handler(9),'Enable','off');
    set(handler(10),'Enable','off');
    
    % Erzeuge 1kHz Referenz Tone
    A_1kHz=A_p0*10^(Phon/20);
    y_1kHz=A_1kHz*sin(2*pi*1000*t).*envelope;
    
    % Erzeuge andere Tone
    A=A_p0*10^(Phon_current/20); 
    y=A*sin(2*pi*f_axis(Stage)*t).*envelope;
    
    logText(['A=',num2str(A),', f=',num2str(f_axis(Stage)),'Hz']);
    
    sound(y_1kHz,FS)
    pause(1)
    sound(y,FS)
    pause(1)
            
    % Enable all objects
    set(handler(7),'Enable','on');
    set(handler(9),'Enable','on');
    set(handler(10),'Enable','on');
end