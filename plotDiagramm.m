function plotDiagramm()
global handler A_array A_p0 f_axis

L_array=20.*log10(A_array./A_p0)% Calculate L(f)
L_threshold=L_array;

%Plot hearing threshold diagram L_th(f)
plot(f_axis,L_threshold,'o')
hold on
legend('Absolute Hearing Threshold')
plot(f_axis,L_threshold)
grid on
set(gca, 'XScale','log') ;
ylabel('Sound Preassure Level L in dB')
xlabel('f in Hz')
title(['Hearing Threshold and Equal Loudness Contours L(f)'])


end